package com.iskar.colorblender;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.SeekBar;

public class MainActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener {
    int Red;
    int Green;
    int Blue;

    SeekBar fSeekBar;
    SeekBar sSeekBar;
    SeekBar faSeekBar;
    SeekBar saSeekBar;

    public int[] calcColor(int progress) {
        if (progress == 0) {
            Red = 255;
            Green = 0;
            Blue = 0;
        }
        if (progress > 0 & progress <= 42) {
            Red = 255;
            Green = progress * 6 - 1;
            Blue = 0;
        } else if (progress > 42 & progress <= 84) {
            Red = 256 - (progress - 42) * 6 - 1;
            Green = 255;
            Blue = 0;
        } else if (progress > 84 & progress <= 126) {
            Red = 0;
            Green = 255;
            Blue = (progress - 84) * 6 - 1;
        } else if (progress > 126 & progress <= 168) {
            Red = 0;
            Green = 256 - (progress - 126) * 6 - 1;
            Blue = 255;
        } else if (progress > 168 & progress <= 210) {
            Red = (progress - 168) * 6 - 1;
            Green = 0;
            Blue = 255;
        } else if (progress > 210 & progress <= 252) {
            Red = 255;
            Green = 0;
            Blue = 256 - (progress - 210) * 6 - 1;
        }
        int[] color = {Red, Green, Blue};
        return color;
    }

    public int[] calcColor(int[] color1, int[] color2, int ALPHA1, int ALPHA2) {
        int[] color = {(color1[0] + color2[0]) / 2, (color1[1] + color2[1]) / 2, (color1[2] + color2[2]) / 2, (ALPHA1 + ALPHA2) / 2};
        return color;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fSeekBar = (SeekBar) findViewById(R.id.fSeekBar);
        sSeekBar = (SeekBar) findViewById(R.id.sSeekBar);
        faSeekBar = (SeekBar) findViewById(R.id.faSeekBar);
        saSeekBar = (SeekBar) findViewById(R.id.saSeekBar);
        fSeekBar.setOnSeekBarChangeListener(this);
        sSeekBar.setOnSeekBarChangeListener(this);
        faSeekBar.setOnSeekBarChangeListener(this);
        saSeekBar.setOnSeekBarChangeListener(this);
    }

    @Override
    public void onProgressChanged(@NonNull SeekBar seekBar, int progress, boolean fromUser) {
        ImageView sColor = (ImageView) findViewById(R.id.imageView4);
        ImageView fColor = (ImageView) findViewById(R.id.imageView3);
        ImageView rColor = (ImageView) findViewById(R.id.imageView5);

        switch (seekBar.getId()) {
            case R.id.fSeekBar: {
                int color1[] = calcColor(fSeekBar.getProgress());
                int color2[] = calcColor(sSeekBar.getProgress());
                int colorR[] = calcColor(color1, color2, faSeekBar.getProgress(), saSeekBar.getProgress());

                fColor.setBackgroundColor(Color.argb(faSeekBar.getProgress(), color1[0], color1[1], color1[2]));
                fColor.invalidate();
                rColor.setBackgroundColor(Color.argb(colorR[3], colorR[0], colorR[1], colorR[2]));
                rColor.invalidate();
                break;
            }
            case R.id.sSeekBar: {
                int color1[] = calcColor(fSeekBar.getProgress());
                int color2[] = calcColor(sSeekBar.getProgress());
                int colorR[] = calcColor(color1, color2, faSeekBar.getProgress(), saSeekBar.getProgress());

                sColor.setBackgroundColor(Color.argb(saSeekBar.getProgress(), color2[0], color2[1], color2[2]));
                sColor.invalidate();
                rColor.setBackgroundColor(Color.argb(colorR[3], colorR[0], colorR[1], colorR[2]));
                rColor.invalidate();
                break;
            }
            case R.id.faSeekBar: {
                int color1[] = calcColor(fSeekBar.getProgress());
                int color2[] = calcColor(sSeekBar.getProgress());
                int colorR[] = calcColor(color1, color2, faSeekBar.getProgress(), saSeekBar.getProgress());

                fColor.setBackgroundColor(Color.argb(faSeekBar.getProgress(), color1[0], color1[1], color1[2]));
                fColor.invalidate();
                rColor.setBackgroundColor(Color.argb(colorR[3], colorR[0], colorR[1], colorR[2]));
                rColor.invalidate();
                break;
            }
            case R.id.saSeekBar: {
                int color1[] = calcColor(fSeekBar.getProgress());
                int color2[] = calcColor(sSeekBar.getProgress());
                int colorR[] = calcColor(color1, color2, faSeekBar.getProgress(), saSeekBar.getProgress());

                sColor.setBackgroundColor(Color.argb(saSeekBar.getProgress(), color2[0], color2[1], color2[2]));
                sColor.invalidate();
                rColor.setBackgroundColor(Color.argb(colorR[3], colorR[0], colorR[1], colorR[2]));
                rColor.invalidate();
                break;
            }

        }

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}


